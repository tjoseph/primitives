Some notes how to manage this repository and [Docker `images` repository](https://gitlab.com/datadrivendiscovery/images).

# Upgrading a fixed dependency

* In `images` repository:
  * If a core package has changed, in `libs` and `libs-lite` images update `D3M_INTERFACE_VERSION`
    variable to the new core package version in `ubuntu-bionic-python36-stable.dockerfile` Dockerfile.
    * Check the `ubuntu-bionic-python36-devel.dockerfile` Dockerfile if there is anything to add from it to the
      `ubuntu-bionic-python36-stable.dockerfile` Dockerfile (any changes necessary to make the new release work).
  * If any of the other fixed package versions has changed, update `libs` and `libs-lite` Dockerfiles
    to install those versions. Make sure CUDA and other dependencies are correct
    for those new versions.
  * For `primitives` image, check the `ubuntu-bionic-python36-devel.dockerfile` Dockerfile if there is anything
    to add from it to the `ubuntu-bionic-python36-stable.dockerfile` Dockerfile (any changes necessary to make
    the new release work).
* In `primitives` repository:
  * Update `FIXED_PACKAGE_VERSIONS` in `run_validation.py` script for new fixed package
    versions (should match those in the `libs` and `libs-lite` Dockerfiles).
  * Make sure that common primitives and sklearn wrap primitives still work. Or update their primitive annotations as necessary.

# Disabling a primitive annotation

Sometimes an accepted primitive annotations starts failing. For example,
a Docker image cannot be built anymore. This happens for various reasons,
a common one is that a primitive does not specify an upper bound on a
dependency and a new version of a dependency got released which breaks the
primitive. In this case we disable the primitive annotation and remove it
from the image by moving it to `failed` directory. There is a script to help
with this, `disable.py`. It can run in two ways:

* Providing a primitive ID from validation log, found listed in the final
  `ERROR` line during validation, example:

    ```
    ./disable.py d3m.primitives.dsbox.MultiTableFeaturization/0.1.3
    ```
* Providing a path to primitive annotation inside the repository, example:

    ```
    ./disable.py ./primitives/ISI/d3m.primitives.dsbox.RandomProjectionTimeSeriesFeaturization/0.1.3/primitive.json
    ```

Do not forget to commit both the removed and added files (they have been moved).
Make sure you do not forget to add any git LFS files on new location (ideally, change
paths of moved files in `.gitattributes` file).
