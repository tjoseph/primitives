{
    "id": "be2ecea2-bae0-4a11-8493-005769b41d5f",
    "version": "0.1.0",
    "name": "Diagonal Multivariate Normal Distribution primitive",
    "description": "Primitive which allows fitting, and sampling from, a multivariate Gaussian (with diagonal covariance matrix)\n-------------\nInputs:  DataFrame of features of shape: NxM, where N = samples and M = features.\nOutputs: DataFrame of features of shape: NxM, where N = samples and M = features.\n-------------\n\nAttributes\n----------\nmetadata:\n    Primitive's metadata. Available as a class attribute.\nlogger:\n    Primitive's logger. Available as a class attribute.\nhyperparams:\n    Hyperparams passed to the constructor.\nrandom_seed:\n    Random seed passed to the constructor.\ndocker_containers:\n    A dict mapping Docker image keys from primitive's metadata to (named) tuples containing\n    container's address under which the container is accessible by the primitive, and a\n    dict mapping exposed ports to ports on that address.\nvolumes:\n    A dict mapping volume keys from primitive's metadata to file and directory paths\n    where downloaded and extracted files are available to the primitive.\ntemporary_directory:\n    An absolute path to a temporary directory a primitive can use to store any files\n    for the duration of the current pipeline run phase. Directory is automatically\n    cleaned up after the current pipeline run phase finishes.",
    "python_path": "d3m.primitives.operator.diagonal_mvn.UBC",
    "primitive_family": "OPERATOR",
    "algorithm_types": [
        "NORMAL_DISTRIBUTION"
    ],
    "source": {
        "name": "UBC",
        "contact": "mailto:tonyjos@ubc.cs.ca",
        "uris": [
            "https://github.com/plai-group/ubc_primitives.git"
        ]
    },
    "keywords": [
        "normal",
        "distribution"
    ],
    "installation": [
        {
            "type": "PIP",
            "package_uri": "git+https://github.com/plai-group/ubc_primitives.git@1ddfe7754d2bf5f2ea94ceb2050efd9b689e7c23#egg=ubc_primitives"
        }
    ],
    "schema": "https://metadata.datadrivendiscovery.org/schemas/v0/primitive.json",
    "original_python_path": "primitives_ubc.diagonalMVN.diagonal_mvn.DiagonalMVNPrimitive",
    "primitive_code": {
        "class_type_arguments": {
            "Inputs": "d3m.container.pandas.DataFrame",
            "Outputs": "d3m.container.pandas.DataFrame",
            "Params": "primitives_ubc.diagonalMVN.diagonal_mvn.Params",
            "Hyperparams": "primitives_ubc.diagonalMVN.diagonal_mvn.Hyperparams"
        },
        "interfaces_version": "2020.5.18",
        "interfaces": [
            "base.ProbabilisticCompositionalityMixin",
            "base.GradientCompositionalityMixin",
            "base.SamplingCompositionalityMixin",
            "supervised_learning.SupervisedLearnerPrimitiveBase",
            "base.PrimitiveBase"
        ],
        "hyperparams": {
            "alpha": {
                "type": "d3m.metadata.hyperparams.Hyperparameter",
                "default": 0.01,
                "structural_type": "float",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "initial fitting step size"
            },
            "beta": {
                "type": "d3m.metadata.hyperparams.Hyperparameter",
                "default": 1e-08,
                "structural_type": "float",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "see reference for details"
            },
            "batch_size": {
                "type": "d3m.metadata.hyperparams.Hyperparameter",
                "default": 1000,
                "structural_type": "int",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "if there is a lot of data, primitive fits using gradient descent in which case, specify batch size here"
            },
            "num_iterations": {
                "type": "d3m.metadata.hyperparams.Hyperparameter",
                "default": 100,
                "structural_type": "int",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "Number of iterations to sample the model."
            }
        },
        "arguments": {
            "hyperparams": {
                "type": "primitives_ubc.diagonalMVN.diagonal_mvn.Hyperparams",
                "kind": "RUNTIME"
            },
            "random_seed": {
                "type": "int",
                "kind": "RUNTIME",
                "default": 0
            },
            "gradient_outputs": {
                "type": "d3m.primitive_interfaces.base.Gradients[d3m.container.pandas.DataFrame]",
                "kind": "RUNTIME"
            },
            "fine_tune": {
                "type": "bool",
                "kind": "RUNTIME",
                "default": false
            },
            "fine_tune_learning_rate": {
                "type": "float",
                "kind": "RUNTIME",
                "default": 1e-05
            },
            "fine_tune_weight_decay": {
                "type": "float",
                "kind": "RUNTIME",
                "default": 1e-05
            },
            "timeout": {
                "type": "typing.Union[NoneType, float]",
                "kind": "RUNTIME",
                "default": null
            },
            "iterations": {
                "type": "typing.Union[NoneType, int]",
                "kind": "RUNTIME",
                "default": null
            },
            "produce_methods": {
                "type": "typing.Sequence[str]",
                "kind": "RUNTIME"
            },
            "inputs": {
                "type": "d3m.container.pandas.DataFrame",
                "kind": "PIPELINE"
            },
            "outputs": {
                "type": "d3m.container.pandas.DataFrame",
                "kind": "PIPELINE"
            },
            "num_samples": {
                "type": "int",
                "kind": "RUNTIME",
                "default": 1
            },
            "temperature": {
                "type": "float",
                "kind": "RUNTIME",
                "default": 0
            },
            "params": {
                "type": "primitives_ubc.diagonalMVN.diagonal_mvn.Params",
                "kind": "RUNTIME"
            }
        },
        "class_methods": {},
        "instance_methods": {
            "__init__": {
                "kind": "OTHER",
                "arguments": [
                    "hyperparams",
                    "random_seed"
                ],
                "returns": "NoneType"
            },
            "backward": {
                "kind": "OTHER",
                "arguments": [
                    "gradient_outputs",
                    "fine_tune",
                    "fine_tune_learning_rate",
                    "fine_tune_weight_decay"
                ],
                "returns": "typing.Tuple[d3m.primitive_interfaces.base.Gradients[d3m.container.pandas.DataFrame], d3m.primitive_interfaces.base.Gradients[primitives_ubc.diagonalMVN.diagonal_mvn.Params]]",
                "description": "Returns the gradient with respect to inputs and with respect to params of a loss\nthat is being backpropagated end-to-end in a pipeline.\n\nThis is the standard backpropagation algorithm: backpropagation needs to be preceded by a\nforward propagation (``forward`` method call).\n\nParameters\n----------\ngradient_outputs:\n    The gradient of the loss with respect to this primitive's output. During backpropagation,\n    this comes from the next primitive in the pipeline, i.e., the primitive whose input\n    is the output of this primitive during the forward execution with ``forward`` (and ``produce``).\nfine_tune:\n    If ``True``, executes a fine-tuning gradient descent step as a part of this call.\n    This provides the most straightforward way of end-to-end training/fine-tuning.\nfine_tune_learning_rate:\n    Learning rate for end-to-end training/fine-tuning gradient descent steps.\nfine_tune_weight_decay:\n    L2 regularization (weight decay) coefficient for end-to-end training/fine-tuning gradient\n    descent steps.\n\nReturns\n-------\nA tuple of the gradient with respect to inputs and with respect to params."
            },
            "fit": {
                "kind": "OTHER",
                "arguments": [
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.CallResult[NoneType]",
                "description": "Fits primitive using inputs and outputs (if any) using currently set training data.\n\nThe returned value should be a ``CallResult`` object with ``value`` set to ``None``.\n\nIf ``fit`` has already been called in the past on different training data,\nthis method fits it **again from scratch** using currently set training data.\n\nOn the other hand, caller can call ``fit`` multiple times on the same training data\nto continue fitting.\n\nIf ``fit`` fully fits using provided training data, there is no point in making further\ncalls to this method with same training data, and in fact further calls can be noops,\nor a primitive can decide to fully refit from scratch.\n\nIn the case fitting can continue with same training data (even if it is maybe not reasonable,\nbecause the internal metric primitive is using looks like fitting will be degrading), if ``fit``\nis called again (without setting training data), the primitive has to continue fitting.\n\nCaller can provide ``timeout`` information to guide the length of the fitting process.\nIdeally, a primitive should adapt its fitting process to try to do the best fitting possible\ninside the time allocated. If this is not possible and the primitive reaches the timeout\nbefore fitting, it should raise a ``TimeoutError`` exception to signal that fitting was\nunsuccessful in the given time. The state of the primitive after the exception should be\nas the method call has never happened and primitive should continue to operate normally.\nThe purpose of ``timeout`` is to give opportunity to a primitive to cleanly manage\nits state instead of interrupting execution from outside. Maintaining stable internal state\nshould have precedence over respecting the ``timeout`` (caller can terminate the misbehaving\nprimitive from outside anyway). If a longer ``timeout`` would produce different fitting,\nthen ``CallResult``'s ``has_finished`` should be set to ``False``.\n\nSome primitives have internal fitting iterations (for example, epochs). For those, caller\ncan provide how many of primitive's internal iterations should a primitive do before returning.\nPrimitives should make iterations as small as reasonable. If ``iterations`` is ``None``,\nthen there is no limit on how many iterations the primitive should do and primitive should\nchoose the best amount of iterations on its own (potentially controlled through\nhyper-parameters). If ``iterations`` is a number, a primitive has to do those number of\niterations (even if not reasonable), if possible. ``timeout`` should still be respected\nand potentially less iterations can be done because of that. Primitives with internal\niterations should make ``CallResult`` contain correct values.\n\nFor primitives which do not have internal iterations, any value of ``iterations``\nmeans that they should fit fully, respecting only ``timeout``.\n\nParameters\n----------\ntimeout:\n    A maximum time this primitive should be fitting during this method call, in seconds.\niterations:\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nA ``CallResult`` with ``None`` value."
            },
            "fit_multi_produce": {
                "kind": "OTHER",
                "arguments": [
                    "produce_methods",
                    "inputs",
                    "outputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.MultiCallResult",
                "description": "A method calling ``fit`` and after that multiple produce methods at once.\n\nThis method allows primitive author to implement an optimized version of both fitting\nand producing a primitive on same data.\n\nIf any additional method arguments are added to primitive's ``set_training_data`` method\nor produce method(s), or removed from them, they have to be added to or removed from this\nmethod as well. This method should accept an union of all arguments accepted by primitive's\n``set_training_data`` method and produce method(s) and then use them accordingly when\ncomputing results.\n\nThe default implementation of this method just calls first ``set_training_data`` method,\n``fit`` method, and all produce methods listed in ``produce_methods`` in order and is\npotentially inefficient.\n\nParameters\n----------\nproduce_methods:\n    A list of names of produce methods to call.\ninputs:\n    The inputs given to ``set_training_data`` and all produce methods.\noutputs:\n    The outputs given to ``set_training_data``.\ntimeout:\n    A maximum time this primitive should take to both fit the primitive and produce outputs\n    for all produce methods listed in ``produce_methods`` argument, in seconds.\niterations:\n    How many of internal iterations should the primitive do for both fitting and producing\n    outputs of all produce methods.\n\nReturns\n-------\nA dict of values for each produce method wrapped inside ``MultiCallResult``."
            },
            "forward": {
                "kind": "OTHER",
                "arguments": [
                    "inputs"
                ],
                "returns": "d3m.container.pandas.DataFrame",
                "description": "Similar to ``produce`` method but it is meant to be used for a forward pass during\nbackpropagation-based end-to-end training. Primitive can implement it differently\nthan ``produce``, e.g., forward pass during training can enable dropout layers, or\n``produce`` might not compute gradients while ``forward`` does.\n\nBy default it calls ``produce`` for one iteration.\n\nParameters\n----------\ninputs:\n    The inputs of shape [num_inputs, ...].\n\nReturns\n-------\nThe outputs of shape [num_inputs, ...]."
            },
            "get_params": {
                "kind": "OTHER",
                "arguments": [],
                "returns": "primitives_ubc.diagonalMVN.diagonal_mvn.Params",
                "description": "Returns parameters of this primitive.\n\nParameters are all parameters of the primitive which can potentially change during a life-time of\na primitive. Parameters which cannot are passed through constructor.\n\nParameters should include all data which is necessary to create a new instance of this primitive\nbehaving exactly the same as this instance, when the new instance is created by passing the same\nparameters to the class constructor and calling ``set_params``.\n\nNo other arguments to the method are allowed (except for private arguments).\n\nReturns\n-------\nAn instance of parameters."
            },
            "gradient_output": {
                "kind": "OTHER",
                "arguments": [
                    "outputs",
                    "inputs"
                ],
                "returns": "d3m.primitive_interfaces.base.Gradients[d3m.container.pandas.DataFrame]",
                "description": "Calculates gradient of log(normal_density(self._mean, self._covariance)) * fit_term_temperature with respect to output.\n\nParameters\n----------\noutputs:\n    The outputs.\ninputs:\n    The inputs.\n\nReturns\n-------\nA structure similar to ``Container`` but the values are of type ``Optional[float]``."
            },
            "gradient_params": {
                "kind": "OTHER",
                "arguments": [
                    "outputs",
                    "inputs"
                ],
                "returns": "d3m.primitive_interfaces.base.Gradients[primitives_ubc.diagonalMVN.diagonal_mvn.Params]",
                "description": "Calculates gradient of log(normal_density(self._mean, self._covariance)) * fit_term_temperature with respect to params.\n\nParameters\n----------\noutputs:\n    The outputs.\ninputs:\n    The inputs.\n\nReturns\n-------\nA version of ``Params`` with all differentiable fields from ``Params`` and values set to gradient for each parameter."
            },
            "log_likelihood": {
                "kind": "OTHER",
                "arguments": [
                    "outputs",
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.CallResult[float]",
                "description": "Calculates log(normal_density(self._mean, self._covariance)).\n\nParameters\n----------\noutputs:\n    The outputs. The number of samples should match ``inputs``.\ninputs:\n    The inputs. The number of samples should match ``outputs``.\ntimeout:\n    A maximum time this primitive should take to produce outputs during this method call, in seconds.\niterations:\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nsum_i(log(p(output_i | input_i, params))) wrapped inside ``CallResult``.\nThe number of returned samples is always 1.\nThe number of columns should match the number of target columns in ``outputs``."
            },
            "log_likelihoods": {
                "kind": "OTHER",
                "arguments": [
                    "outputs",
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.CallResult[d3m.container.numpy.ndarray]",
                "description": "Returns log probability of outputs given inputs and params under this primitive:\n\nlog(p(output_i | input_i, params))\n\nParameters\n----------\noutputs:\n    The outputs. The number of samples should match ``inputs``.\ninputs:\n    The inputs. The number of samples should match ``outputs``.\ntimeout:\n    A maximum time this primitive should take to produce outputs during this method call, in seconds.\niterations:\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nlog(p(output_i | input_i, params))) wrapped inside ``CallResult``.\nThe number of columns should match the number of target columns in ``outputs``."
            },
            "multi_produce": {
                "kind": "OTHER",
                "arguments": [
                    "produce_methods",
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.MultiCallResult",
                "description": "A method calling multiple produce methods at once.\n\nWhen a primitive has multiple produce methods it is common that they might compute the\nsame internal results for same inputs but return different representations of those results.\nIf caller is interested in multiple of those representations, calling multiple produce\nmethods might lead to recomputing same internal results multiple times. To address this,\nthis method allows primitive author to implement an optimized version which computes\ninternal results only once for multiple calls of produce methods, but return those different\nrepresentations.\n\nIf any additional method arguments are added to primitive's produce method(s), they have\nto be added to this method as well. This method should accept an union of all arguments\naccepted by primitive's produce method(s) and then use them accordingly when computing\nresults.\n\nThe default implementation of this method just calls all produce methods listed in\n``produce_methods`` in order and is potentially inefficient.\n\nIf primitive should have been fitted before calling this method, but it has not been,\nprimitive should raise a ``PrimitiveNotFittedError`` exception.\n\nParameters\n----------\nproduce_methods:\n    A list of names of produce methods to call.\ninputs:\n    The inputs given to all produce methods.\ntimeout:\n    A maximum time this primitive should take to produce outputs for all produce methods\n    listed in ``produce_methods`` argument, in seconds.\niterations:\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nA dict of values for each produce method wrapped inside ``MultiCallResult``."
            },
            "produce": {
                "kind": "PRODUCE",
                "arguments": [
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.CallResult[d3m.container.pandas.DataFrame]",
                "singleton": false,
                "inputs_across_samples": [],
                "description": "Produce primitive's best choice of the output for each of the inputs.\n\nThe output value should be wrapped inside ``CallResult`` object before returning.\n\nIn many cases producing an output is a quick operation in comparison with ``fit``, but not\nall cases are like that. For example, a primitive can start a potentially long optimization\nprocess to compute outputs. ``timeout`` and ``iterations`` can serve as a way for a caller\nto guide the length of this process.\n\nIdeally, a primitive should adapt its call to try to produce the best outputs possible\ninside the time allocated. If this is not possible and the primitive reaches the timeout\nbefore producing outputs, it should raise a ``TimeoutError`` exception to signal that the\ncall was unsuccessful in the given time. The state of the primitive after the exception\nshould be as the method call has never happened and primitive should continue to operate\nnormally. The purpose of ``timeout`` is to give opportunity to a primitive to cleanly\nmanage its state instead of interrupting execution from outside. Maintaining stable internal\nstate should have precedence over respecting the ``timeout`` (caller can terminate the\nmisbehaving primitive from outside anyway). If a longer ``timeout`` would produce\ndifferent outputs, then ``CallResult``'s ``has_finished`` should be set to ``False``.\n\nSome primitives have internal iterations (for example, optimization iterations).\nFor those, caller can provide how many of primitive's internal iterations\nshould a primitive do before returning outputs. Primitives should make iterations as\nsmall as reasonable. If ``iterations`` is ``None``, then there is no limit on\nhow many iterations the primitive should do and primitive should choose the best amount\nof iterations on its own (potentially controlled through hyper-parameters).\nIf ``iterations`` is a number, a primitive has to do those number of iterations,\nif possible. ``timeout`` should still be respected and potentially less iterations\ncan be done because of that. Primitives with internal iterations should make\n``CallResult`` contain correct values.\n\nFor primitives which do not have internal iterations, any value of ``iterations``\nmeans that they should run fully, respecting only ``timeout``.\n\nIf primitive should have been fitted before calling this method, but it has not been,\nprimitive should raise a ``PrimitiveNotFittedError`` exception.\n\nParameters\n----------\ninputs:\n    The inputs of shape [num_inputs, ...].\ntimeout:\n    A maximum time this primitive should take to produce outputs during this method call, in seconds.\niterations:\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nThe outputs of shape [num_inputs, ...] wrapped inside ``CallResult``."
            },
            "sample": {
                "kind": "OTHER",
                "arguments": [
                    "inputs",
                    "num_samples",
                    "timeout",
                    "iterations"
                ],
                "returns": "typing.Sequence[d3m.container.pandas.DataFrame]",
                "description": "Sample output for each input from ``inputs`` ``num_samples`` times.\n\nSemantics of ``timeout`` and ``iterations`` is the same as in ``produce``.\n\nParameters\n----------\ninputs:\n    The inputs of shape [num_inputs, ...].\nnum_samples:\n    The number of samples to return in a set of samples.\ntimeout:\n    A maximum time this primitive should take to sample outputs during this method call, in seconds.\niterations:\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nThe multiple sets of samples of shape [num_samples, num_inputs, ...] wrapped inside\n``CallResult``. While the output value type is specified as ``Sequence[Outputs]``, the\noutput value can be in fact any container type with dimensions/shape equal to combined\n``Sequence[Outputs]`` dimensions/shape. Subclasses should specify which exactly type\nthe output is."
            },
            "set_fit_term_temperature": {
                "kind": "OTHER",
                "arguments": [
                    "temperature"
                ],
                "returns": "NoneType",
                "description": "Sets the temperature used in ``gradient_output`` and ``gradient_params``.\n\nParameters\n----------\ntemperature:\n    The temperature to use, [0, inf), typically, [0, 1]."
            },
            "set_params": {
                "kind": "OTHER",
                "arguments": [
                    "params"
                ],
                "returns": "NoneType",
                "description": "Sets parameters of this primitive.\n\nParameters are all parameters of the primitive which can potentially change during a life-time of\na primitive. Parameters which cannot are passed through constructor.\n\nNo other arguments to the method are allowed (except for private arguments).\n\nParameters\n----------\nparams:\n    An instance of parameters."
            },
            "set_training_data": {
                "kind": "OTHER",
                "arguments": [
                    "inputs",
                    "outputs"
                ],
                "returns": "NoneType",
                "description": "Sets current training data of this primitive.\n\nThis marks training data as changed even if new training data is the same as\nprevious training data.\n\nStandard sublasses in this package do not adhere to the Liskov substitution principle when\ninheriting this method because they do not necessary accept all arguments found in the base\nclass. This means that one has to inspect which arguments are accepted at runtime, or in\nother words, one has to inspect which exactly subclass a primitive implements, if\nyou are accepting a wider range of primitives. This relaxation is allowed only for\nstandard subclasses found in this package. Primitives themselves should not break\nthe Liskov substitution principle but should inherit from a suitable base class.\n\nParameters\n----------\ninputs:\n    The inputs.\noutputs:\n    The outputs."
            }
        },
        "class_attributes": {
            "logger": "logging.Logger",
            "metadata": "d3m.metadata.base.PrimitiveMetadata"
        },
        "instance_attributes": {
            "hyperparams": "d3m.metadata.hyperparams.Hyperparams",
            "random_seed": "int",
            "docker_containers": "typing.Dict[str, d3m.primitive_interfaces.base.DockerContainer]",
            "volumes": "typing.Dict[str, str]",
            "temporary_directory": "typing.Union[NoneType, str]"
        },
        "params": {
            "mean": "d3m.container.numpy.ndarray",
            "covariance": "d3m.container.numpy.ndarray"
        }
    },
    "structural_type": "primitives_ubc.diagonalMVN.diagonal_mvn.DiagonalMVNPrimitive",
    "digest": "7672c9181008d4b247cbc23d40ed519872fdd2db5a8758703a7c31fa6613d89b"
}
